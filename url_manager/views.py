from django.shortcuts import render
from url_manager.models import Url
from .tools import make_str_url_parse_info, make_str_url_parse_data


def index(request):
    template = 'index.html'
    urls = Url.objects.all().order_by('date_create').reverse()
    info = u''
    data = u''
    for url in urls:
        info += make_str_url_parse_info(url)
        data += make_str_url_parse_data(url)
    context = {
        'url_info': info,
        'url_data': data
    }

    return render(request, template, context)

