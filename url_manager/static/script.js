/**
 * Created by Alwx on 25/02/16.
 */

document.addEventListener('DOMContentLoaded', function () {
	var ws = new WebSocket('ws://localhost:8080/websocket'),
		info_area = document.getElementById('info'),
		data_area = document.getElementById('data'),
		hello = document.getElementById('hello');


	ws.onopen = function () {
		console.log('Connected to websocket')
	};
	ws.onmessage = function (e) {
		var data = JSON.parse(e.data);
		if (data.url_info) {
			info_area.value =  data.url_info + info_area.value
		}
		if (data.url_data) {
			data_area.value = data.url_data + data_area.value
		}
		if (hello) {
			hello.parentNode.removeChild(hello);
			hello = undefined;
		}
	};

	ws.onclose = function() {
		console.log('Connection to websocket closed')
	}
});