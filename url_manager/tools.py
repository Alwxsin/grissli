# coding=utf-8
import json
import cgi
from .models import Url


def make_str_url_parse_info(url):
    result = u'<дата '
    date = url.exec_time.strftime('%d.%m.%Y %H:%M:%S')

    if url.success_parse is None:
        parse = u'Not parsed yet'
    elif url.success_parse is True:
        parse = u'Parsed successfully'
    else:
        parse = u'Parsed unsuccessfully'

    result += date + u'>: ' + url.url + u'; ' + parse + u'\n'
    return result


def make_str_url_parse_data(url):
    result = url.url + u' - '

    if url.data:
        data = json.loads(url.data)
        data = {
            'title': data['title'] if 'title' in data else '',
            'encoding': data['encoding'] if 'encoding' in data else '',
            'h1': data['h1'] if 'h1' in data else ''
        }
        result += u'Title: ' + data['title'] + u';  Encoding: ' + data['encoding'] + u'; H1: ' + data['h1'] + u'\n'
    elif not url.success_parse:
        result += u'; No data\n'
    else:
        result += u'; Not parsed yet\n'

    return result


def get_encoding_from_headers(content_type):
    if not content_type:
        return None

    content_type, params = cgi.parse_header(content_type)

    if 'charset' in params:
        return params['charset'].strip("'\"")

    if 'text' in content_type:
        return 'ISO-8859-1'


def get_timedelta_in_seconds(d1, d2):
    date = d1 - d2
    if not date.days < 0:
        return date.days * 86400 + date.seconds
    else:
        return 0
