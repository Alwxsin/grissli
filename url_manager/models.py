from datetime import timedelta
from django.db import models
from django.utils import timezone
from django.core.validators import URLValidator


class Url(models.Model):
    url = models.URLField(max_length=200, default='http://', validators=[URLValidator],
                          help_text="Url should starts with http[s]://")
    time_shift = models.IntegerField(verbose_name="Time Shift in seconds", blank=True,
                                     default=0, help_text="If you need minutes click on checkbox next")
    minutes = models.BooleanField(default=False)

    date_create = models.DateTimeField(auto_now_add=True)
    exec_time = models.DateTimeField(verbose_name="Execution time")
    parsed = models.BooleanField(verbose_name="Parsed", default=False)
    data = models.TextField(verbose_name="Serialized data", blank=True)
    success_parse = models.NullBooleanField(verbose_name="Successfully parse", null=True)

    def __str__(self):
        return self.url

    def save(self, *args, **kwargs):
        if not self.time_shift:
            self.time_shift = 0
        seconds = self.time_shift
        if self.minutes:
            seconds *= 60
        if not self.id:
            self.exec_time = timezone.now() + timedelta(seconds=seconds)
        super(Url, self).save(*args, **kwargs)

    def set_parsed(self):
        self.parsed = True
        self.save()

    def parsed_successfully(self):
        self.success_parse = True
        self.parsed = True

    def parsed_unsuccessfully(self):
        self.success_parse = False
        self.parsed = True

    @staticmethod
    def get_not_parsed():
        return Url.objects.filter(parsed=False)
