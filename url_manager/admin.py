from django.contrib import admin
from .models import Url


class UrlAdmin(admin.ModelAdmin):
    fields = ('url', ('time_shift', 'minutes'))
    exclude = ('exec_time', 'parsed', 'data', 'success_parse')

admin.site.register(Url, UrlAdmin)
