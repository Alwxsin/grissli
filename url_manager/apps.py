from __future__ import unicode_literals

from django.apps import AppConfig


class UrlManagerConfig(AppConfig):
    name = 'url_manager'
