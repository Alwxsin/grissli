import re
import json
import os
import datetime

import django
import django.conf
import django.contrib.auth
import django.core.handlers.wsgi
import django.db

import tornado.web
import tornado.websocket
import tornado.wsgi
import tornado.httpclient
from tornado import ioloop, gen, httpserver
from tornado.options import options, define

from six import print_

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "grissli.settings")
port = 8080
define('port', type=int, default=port)
django.setup()

from url_manager.models import Url
from url_manager.tools import *

wss = []
urls_to_fetch = []
urls_in_work = []
http_client = tornado.httpclient.AsyncHTTPClient()


def ws_send(message):
    for ws in wss:
        if not ws.ws_connection.stream.socket:
            wss.remove(ws)
        else:
            ws.write_message(message)


class WebSocketHandler(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        if self not in wss:
            wss.append(self)
        print_('Websocket connection opened')

    def on_message(self, message):
        print_('Websocket got a message: ' + str(message))

    def on_close(self):
        if self in wss:
            wss.remove(self)
        print_('Websocket closed')


def manage_url_fetch():
    urls = Url.get_not_parsed()
    for url in urls:
        if url not in urls_to_fetch:
            urls_to_fetch.append(url)
            urls_in_work.append(url)

    io_loop = ioloop.IOLoop.current()
    for url in urls_in_work:
        urls_in_work.remove(url)
        now = datetime.datetime.now()
        timeout = get_timedelta_in_seconds(url.exec_time.replace(tzinfo=None), now)
        io_loop.call_later(timeout, fetch_url, url)
    io_loop.add_timeout(datetime.timedelta(seconds=1), manage_url_fetch)


def parse_request(r):
        data = {
            'title': 'Not available',
            'encoding': 'Not available',
            'h1': 'Not available',
        }

        if not r.error:
            title_regex = '<title.*?>(.+?)</title>'
            h1_regex = '<h1.*?>(.+?)</h1>'

            title = re.findall(title_regex, r.body)
            h1 = re.findall(h1_regex, r.body)
            encoding = get_encoding_from_headers(r.headers['Content-Type'])
            if encoding:
                data['encoding'] = encoding
                if title:
                    data['title'] = title[0].decode(encoding=encoding)
                if h1:
                    data['h1'] = h1[0].decode(encoding=encoding)

        return data


@gen.coroutine
def fetch_url(url):
    try:
        response = yield http_client.fetch(url.url)
        data = parse_request(response)
        url.data = json.dumps(data)
        url.parsed_successfully()
        url.save()
        message = {
            'url_info': make_str_url_parse_info(url),
            'url_data': make_str_url_parse_data(url)
        }

        ws_send(message)
    except:
        url.parsed_unsuccessfully()
        url.save()


def main():
    wsgi_app = tornado.wsgi.WSGIContainer(
        django.core.handlers.wsgi.WSGIHandler())
    tornado_app = tornado.web.Application(
        [
            (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': 'url_manager/static/'}),
            ('/websocket', WebSocketHandler),
            ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
        ])
    server = httpserver.HTTPServer(tornado_app)
    server.listen(options.port)

    io_loop = ioloop.IOLoop.current()

    io_loop.add_timeout(datetime.timedelta(seconds=1), manage_url_fetch)
    io_loop.start()


if __name__ == '__main__':
    print_('Start server at port ' + str(port))
    main()
