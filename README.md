# README #

Backend skills test from grissli

### How to install ###

* In terminal use these commands
```
git clone https://Alwxsin@bitbucket.org/Alwxsin/grissli.git
```
* If you want to use virtual environment, then
```
virtualenv .venv
source .venv/bin/activate
```
* To install all requirements
```
pip install -r req.txt
```

* Setup database
```
python manage.py makemigrations
python manage.py migrate
```

* Create admin
```
python manage.py createsuperuser
```

* Finally, run server
```
python tornado_main.py
```
* And open in browser
```
127.0.0.1:8080
```